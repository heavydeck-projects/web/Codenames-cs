
compress:
	find -iname '*.ttf'  | xargs -n1 gzip --keep -9 -f
	find -iname '*.js'   | xargs -n1 gzip --keep -9 -f
	find -iname '*.html' | xargs -n1 gzip --keep -9 -f
	find -iname '*.svg'  | xargs -n1 gzip --keep -9 -f

clean:
	-find -iname '*.gz'  | xargs rm

