"use strict";

// -----------------------
// --- Board generator ---
// -----------------------

// The set-*.js files should have been loaded before this file
// Available files:
//   * set_es_base
//   * set_es_furry
//   * set_intl_base
//   * set_intl_furry

function generateBoard(){
  var rv = [];
  var all_the_words = [];
  
  //Append sets to all_the_words if checked
  if(document.getElementById("check-es-base").checked){
    all_the_words = all_the_words.concat(set_es_base);
  }
  if(document.getElementById("check-es-furry").checked){
    all_the_words = all_the_words.concat(set_es_furry);
  }
  if(document.getElementById("check-intl-base").checked){
    all_the_words = all_the_words.concat(set_intl_base);
  }
  if(document.getElementById("check-intl-furry").checked){
    all_the_words = all_the_words.concat(set_intl_furry);
  }
  
  //if all_the_words is empty, show warning and exit
  if(all_the_words.length == 0){
    alert("Please select at least one word set");
    throw "No words selected";
  }
  
  //Select 25 random words
  for(var i = 0; i < 25; i++){
    var rnd_index = Math.round(Math.random() * (all_the_words.length - 1));
    //Push board[rnd_index] into board_final
    rv.push(all_the_words[rnd_index]);
    //Slice-out the just selected element
    var left_slice = all_the_words.slice(0,rnd_index);
    var right_slice = [];
    if(rnd_index < all_the_words.length){
      right_slice = all_the_words.slice(rnd_index+1, all_the_words.length);
    }
    //board now the concat of both slices
    all_the_words = left_slice.concat(right_slice);
  }
  
  return rv;
}

function makeBoardUrl(){
  var words = generateBoard();
  var secret_arg = "?words=" + JSON.stringify(words);
  var secret_prefix = "board.html"

  console.log(secret_arg);
  //document.getElementById("text-board-url").innerHTML = secret_prefix + secret_arg;
  document.getElementById("text-board-url").innerHTML = "Click to get your board!";
  document.getElementById("text-board-url").href = secret_prefix + secret_arg;
}

// ------------------------
// --- Secret generator ---
// ------------------------
function generateSecret() {
  var blu_board = ["R", "R", "R", "R", "R", "R", "R", "R", "B", "B", "B", "B", "B", "B", "B", "B", "B", "X", "C", "C", "C", "C", "C", "C", "C"];  //<-- Blue starts
  var red_board = ["R", "R", "R", "R", "R", "R", "R", "R", "R", "B", "B", "B", "B", "B", "B", "B", "B", "X", "C", "C", "C", "C", "C", "C", "C"]; //<-- Red starts
  var board = [];

  //Decide who starts
  if(Math.random() > 0.5){
    //Blue
    board = blu_board;
  }
  else{
    //Red
    board = red_board;
  }

  //Shuffle board
  var board_final = [];
  while(board.length > 0){
    //A random index
    var i = Math.round(Math.random() * (board.length - 1));
    //Push board[i] into board_final
    board_final.push(board[i]);
    //Slice-out the just selected element
    var left_slice = board.slice(0,i);
    var right_slice = [];
    if(i < board.length){
      right_slice = board.slice(i+1, board.length);
    }
    //board now the concat of both slices
    board = left_slice.concat(right_slice);
  }

  return board_final;
}

function makeSecretUrl(){
  var secret = generateSecret();
  var secret_arg = "?cells="
  var secret_prefix = "spies.html"
  for(var c of secret){
    secret_arg = secret_arg + c;
  }

  console.log(secret_arg);
  document.getElementById("text-secret-url").innerHTML = secret_prefix + secret_arg;
  document.getElementById("text-secret-url").href = secret_prefix + secret_arg;
}

function generatorMain(){
	console.log("Main");
	document.getElementById("button-make-board").onclick = makeBoardUrl;
	document.getElementById("button-make-secret").onclick = makeSecretUrl;
}

//Call main when page loaded
document.onload = generatorMain();

