"use strict";

var board_cells = [];
var board_stat  = {};

//If set, it will force the next click to flip into a defined state
var state_override = "";

function onCardClicked(event){
  //console.log("Cell " + this.id + " clicked with status: " + board_stat[this.id]);
  updateCell(this.id);
}

function updateCell(cellId) {
  var cell = document.getElementById(cellId);
  var current_cell_stat = board_stat[cellId];

  //Check if we are doing an override
  if(state_override != ""){
    current_cell_stat = state_override;
    state_override = "";
  }

  switch (current_cell_stat){
    case "R":
      //Make cell BLUE
      board_stat[cellId] = "B";
      cell.childNodes[0].className = "card-blue";
      cell.childNodes[0].childNodes[0].style = "visibility: hidden;";
      break;
    case "B":
      //Make cell CIVILIAN
      board_stat[cellId] = "C";
      cell.childNodes[0].childNodes[0].style = "visibility: hidden;";
      cell.childNodes[0].className = "card-plain";
      break;
    case "C":
      //Make cell BLACK
      board_stat[cellId] = "X";
      cell.childNodes[0].childNodes[0].style = "visibility: hidden;";
      cell.childNodes[0].className = "card-black";
      break;
    case "X":
      //Make cell TEXT
      board_stat[cellId] = "T";
      cell.childNodes[0].childNodes[0].style = "visibility: visible;";
      cell.childNodes[0].className = "card-word";
      break;
    case "T":
      //Make cell RED
      board_stat[cellId] = "R";
      cell.childNodes[0].childNodes[0].style = "visibility: hidden;";
      cell.childNodes[0].className = "card-red";
      break;
  }
}

function boardMain() {
  board_cells = getCellArray();

  //Register the onClick event for every cell
  //Set the card word from url
  var url = new URL(window.location.href);
  var cell_arg = url.searchParams.get("words");

  var words = []
  if(cell_arg != null){
    words = JSON.parse(cell_arg);
  }
  else{
    //Default board
    words = words.concat(['Oro', 'Guante', 'Emperador', 'Tubo', 'Bruja']);
    words = words.concat(['Cámara', 'Golondrina', 'Ruleta', 'Iglesia', 'Lima']);
    words = words.concat(['Pirámide', 'Pase', 'Regla', 'Obra', 'Corredor']);
    words = words.concat(['Robot', 'Vampiro', 'Señal', 'Muerte', 'Golfo']);
    words = words.concat(['Rosa', 'Italia', 'Corona', 'Ambulancia', 'Agente']);
  }

  for(var cell of board_cells){
    cell.onclick = onCardClicked;
    //Fill board status where:
    //  * R --> Red
    //  * B --> Blue
    //  * X --> Man in Black
    //  * C --> Civilian
    //  * T --> Text card
    board_stat[cell.id] = "T";
    if(words.length > 0){
      var text_normal = document.createElement("div");
      var text_flip   = document.createElement("div");
      var text_word = words.pop();

      cell.childNodes[0].childNodes[0].innerHTML = "";
      cell.childNodes[0].childNodes[0].appendChild(text_normal);
      cell.childNodes[0].childNodes[0].appendChild(text_flip);

      text_normal.innerHTML = text_word;
      text_flip.innerHTML = text_word;
      text_flip.setAttribute('class', 'flip-text');
    }
  }

  //Register keydown event
  document.addEventListener('keydown', keyDownHandler);
}

function keyDownHandler(event){
  switch(event.key){
    case "r":
    case "R":
    case "1":
    //Make it Red
    console.log("Make next cell RED");
    state_override = "T";
    break;

    case "b":
    case "B":
    case "2":
    //Make it Blue
    console.log("Make next cell BLUE");
    state_override = "R";
    break;

    case "c":
    case "C":
    case "3":
    //Make it Civilian
    console.log("Make next cell CIVILIAN");
    state_override = "B";
    break;

    case "k":
    case "K":
    case "x":
    case "X":
    case "4":
    //Make it Black
    console.log("Make next cell BLACK");
    state_override = "C";
    break;

    case "n":
    case "N":
    case "0":
    //Make it Text
    console.log("Make next cell TEXT");
    state_override = "X";
    break;

  }
}

//Call main when page loaded
document.onload = boardMain();

