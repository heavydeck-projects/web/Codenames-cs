"use strict";

var board_cells = [];

function spiesMain() {
  //Get the board div
  var board = document.getElementById("board");

  board_cells = getCellArray();

  //Get the url argument 'cells'
  var url = new URL(window.location.href);
  var cell_arg = url.searchParams.get("cells");

  //If null, generate one
  if(cell_arg == null){
    cell_arg="RRRRRRRRBBBBBBBBBXCCCCCCC" //<-- Blue starts
    cell_arg="RRRRRRRRRBBBBBBBBXCCCCCCC" //<-- Red starts
  }

  //Fill board with given argument where:
  //  * R --> Red
  //  * B --> Blue
  //  * X --> Man in Black
  //  * C --> Civilian
  var idx = 0;
  var blues = 0;
  var reds  = 0;
  for(var c of cell_arg){
    var img_new = document.createElement("img");
    var skip = false;
    switch(c){
      case "R":
      img_new.src = "svg/sq-red.svg"
      reds++;
      break;
      case "B":
      img_new.src = "svg/sq-blue.svg"
      blues++;
      break;
      case "X":
      img_new.src = "svg/sq-black.svg"
      break;
      case "C":
      img_new.src = "svg/sq-plain.svg"
      break;
      default:
      skip = true;
      break;
    }
    if(skip) continue;

    board_cells[idx].innerText = "";
    board_cells[idx].appendChild(img_new);
    idx++;
  }
  //If reds > blues, Reds start.
  if(reds > blues){
    document.getElementById("heading-text").innerText = "Starting team: RED";
  }
  else{
    document.getElementById("heading-text").innerText = "Starting team: BLUE";
  }
}


//Call main when page loaded
document.onload = spiesMain();
