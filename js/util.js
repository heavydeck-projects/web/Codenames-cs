"use strict";

//make array of all the game cells.
function getCellArray() {
  var rv = [];
  var cols=['a', 'b', 'c', 'd', 'e'];
  var rows=['1', '2', '3', '4', '5'];
  for (var row of rows){
    for (var col of cols){
      rv.push(document.getElementById("box-" + row + col));
    }
  }
  return rv;
}

